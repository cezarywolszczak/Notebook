package kernel;

import java.io.*;

/**
 * Created by HP on 22.12.2015.
 *
 */
public class WriteFile {
    public static void writeBinary(String fileName, String text){
        DataOutputStream dataOutputStream;
        try {
            dataOutputStream = new DataOutputStream(new BufferedOutputStream (new FileOutputStream(fileName)));
            dataOutputStream.writeUTF(text);
            dataOutputStream.close();
        }catch (FileNotFoundException fnfe){
            System.err.println("Nie znaleziono pliku, WriteFile->writeBinary");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void write(String fileName, String text){
        try {
            PrintWriter printWriter = new PrintWriter(fileName);
            printWriter.println(text);
            printWriter.close();
        } catch (FileNotFoundException e) {
            System.err.println("Nie znaleziono pliku, WriteFile->writeBinary");
        }
    }
}
