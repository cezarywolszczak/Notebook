package windows;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Stage;
import kernel.Container;
import kernel.ReadFile;
import kernel.WriteFile;


/**
 * Created by HP on 17.12.2015.
 *
 */

public class Main extends Application {
    static int fontSize = 20;
    private static Stage window;
    private static BorderPane layout;
    private static TextArea mainTextArea;
    private TextField textFieldFind;
    private Button buttonCloseFind;
    private Button buttonNext;

    public static void main(String[] args) {
        launch(args);
    }

    public static BorderPane getLayout() {
        return layout;
    }

    public static TextArea getMainTextArea() {
        return mainTextArea;
    }

    public static void setMainTextArea(TextArea mainTextArea) {
        Main.mainTextArea = mainTextArea;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Notebook");
        window.getIcons().add(new Image("image/notebookIcon.png"));

        //layout, part two at bottom
        layout = new BorderPane();


        //file Menu
        Menu fileMenu = new Menu("File");
        Menu editMenu = new Menu("Edit");
        Menu helpMenu = new Menu("Help");

        //file Items
        fileMenu.getItems().add(new MenuItem("Save file..."));
        fileMenu.getItems().add(new MenuItem("Open file..."));
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Save binary file..."));
        fileMenu.getItems().add(new MenuItem("Open binary file..."));
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Settings..."));
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Quit"));


        //item Save binary file..., fileChoicer..File
        FileChooser fileChooserSave = new FileChooser();
        fileChooserSave.setTitle("Save");
        fileMenu.getItems().get(0).setOnAction(event4 -> {
            try {
                WriteFile.write(fileChooserSave.showSaveDialog(window).toString(), mainTextArea.getText());
            }catch (NullPointerException npe){
                System.err.println("Nie znaleziono pliku, fileChooserSave");
            }
        });

        //item Open file..., fileChoicer..File
        FileChooser fileChooserOpen = new FileChooser();
        fileChooserOpen.setTitle("Open");
        fileMenu.getItems().get(1).setOnAction(event4 -> {
            try {
                mainTextArea.setText(ReadFile.read(fileChooserOpen.showOpenDialog(window).toString()));
            }catch (NullPointerException npe){
                System.err.println("Nie znaleziono pliku, fileChooserOpen");
            }
        });

        //item Save binary file..., fileChoicer..File
        FileChooser fileChooserSaveBinary = new FileChooser();
        fileChooserSaveBinary.setTitle("Save binary");
        fileMenu.getItems().get(3).setOnAction(event2 -> {
            try {
                WriteFile.writeBinary(fileChooserSaveBinary.showSaveDialog(window).toString(), mainTextArea.getText());
            }catch (NullPointerException npe){
                System.err.println("Nie znaleziono pliku, fileChooserSaveBinary");
            }
        });

        //item Open binary file..., fileChoicer..File
        FileChooser fileChooserOpenBinary = new FileChooser();
        fileChooserOpenBinary.setTitle("Open binary");
        fileMenu.getItems().get(4).setOnAction(event1 -> {
            try {
                mainTextArea.setText(ReadFile.readBinary(fileChooserOpenBinary.showOpenDialog(window).toString()));
            }catch (NullPointerException npe){
                System.err.println("Nie znaleziono pliku, fileChooserOpenBinary");
            }
        });

        //item Settings..File
        fileMenu.getItems().get(6).setOnAction(event ->{
            Settings.setSettings();
        });

        //item Exit Item..File
        fileMenu.getItems().get(8).setOnAction(event3 ->
            System.exit(0)
        );

        //file Edit
        editMenu.getItems().add(new MenuItem("Find..."));

        //item Find..Edit
        editMenu.getItems().get(0).setOnAction(event1 -> {
            layout.setBottom(FunctionElements.layoutFind(buttonCloseFind, buttonNext));
        });

        //file Help
        helpMenu.getItems().add(new MenuItem("About program..."));

        //item About program.. ..Help
        helpMenu.setOnAction(event ->
            Info.displayAllert("About program","Title: \n\tNootebook \nVersion: \n\t1.0 \nAuthor: \n\tCezary Wolszczak",300,400)
        );

        //Main menu bar
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, editMenu, helpMenu);

        //mainTextArea TextArea
        mainTextArea = new TextArea();
        mainTextArea.setWrapText(Container.isSetWrapText()); //łamanie textu
        mainTextArea.setStyle("-fx-font-size: " + Container.getFontSize());
//        mainTextArea.setStyle("-fx-tex");

        //layout
        layout.setTop(menuBar);
        layout.setCenter(mainTextArea);

        Scene scene = new Scene(layout, 560,600);
        scene.getStylesheets().add("css/Styles.css");
        window.setScene(scene);
        window.show();

    }

}
