package windows;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import kernel.Container;

public class Settings {

    private static BorderPane layout;
    private static GridPane layoutStart, layoutFont;
    private static Button buttonCancel, buttonApply, buttonOk;
    private static boolean tmp = false;
    private static Stage window;


    public static void setSettings(){
        window = new Stage();
        window.getIcons().add(new Image("image/notebookIcon.png"));

        layout = new BorderPane();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Settings");
        window.setMinWidth(500);
        window.setMinHeight(300);
        window.setMaxWidth(1000);
        window.setMaxHeight(700);

        //info Label
        createStartLayout();

        //TreeItem main tree
        TreeItem<String> treeItem, itemFont, itemFontSize2;
        treeItem = new TreeItem<>("Settings");
        treeItem.setExpanded(true);

        //TreeItem Font size
        itemFont = makeBranch("Font", treeItem);

        TreeView<String> treeView = new TreeView<>(treeItem);
        treeView.getStyleClass().add("treeView");
        treeView.getSelectionModel().selectedItemProperty()
                .addListener((v, oldValue, newValue)->{
                    if (newValue != null)
                        layout.setCenter(setLayout(newValue.getValue()));
                });


        //buttons
        buttonCancel = new Button("Cancel");
        buttonCancel.setMinSize(70,20);
        buttonApply = new Button("Apply");
        buttonApply.setMinSize(70,20);
        buttonOk = new Button("Ok");
        buttonOk.setMinSize(70,20);
        HBox layoutButtons = new HBox();
        layoutButtons.setAlignment(Pos.CENTER_RIGHT);
        layoutButtons.setPadding(new Insets(20,20,20,20));
        layoutButtons.setSpacing(10);
        layoutButtons.getChildren().addAll(buttonOk, buttonApply, buttonCancel);
        buttonOk.setOnAction(event -> {
            window.close();
        });
        buttonCancel.setOnAction(event -> {
            window.close();
        });

        //layout
        layout.setLeft(treeView);
        layout.setCenter(layoutStart);
        layout.setBottom(layoutButtons);

        Scene scene = new Scene(layout, 800, 500);
        scene.getStylesheets().add("css/Styles.css");
        scene.getStylesheets().add("css/SettingStyles.css");
        window.setScene(scene);
        window.show();
    }

    public static TreeItem<String> makeBranch(String title, TreeItem<String> parent){
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(true);
        parent.getChildren().add(item);
        return item;
    }

    public static Pane setLayout(String treeValue){
        switch (treeValue){
            case "Font":
                createFontLayout();
                return layoutFont;
            default:
                return layoutStart;
        }
    }

    public static void createFontLayout(){
        layoutFont = new GridPane();
        layoutFont.setPadding(new Insets(20, 20, 20, 20));

        //choice box text size
        Label labelFontSize = new Label("Font size: \t");
        layoutFont.add(labelFontSize, 0,1);
        ChoiceBox<Integer> choiceBoxFontSize = new ChoiceBox<Integer>();
        choiceBoxFontSize.setItems(FXCollections.observableArrayList(5,6,7,8,9,10,11,12,13,14,15,16,18,24,30,36,48,60));
        choiceBoxFontSize.setValue(Container.getFontSize());
        layoutFont.add(choiceBoxFontSize, 1,1);


        //text wrap
        Label labelWrapText = new Label("Wrap text: \t");
        layoutFont.add(labelWrapText,0,2);
        CheckBox checkBoxWrapText = new CheckBox();
        checkBoxWrapText.setSelected(Container.isSetWrapText());
        layoutFont.add(checkBoxWrapText,1,2);


        buttonOk.setOnAction(event -> {
            Container.setFontSize(choiceBoxFontSize.getValue());
            Container.setSetWrapText(checkBoxWrapText.isSelected());
            Container.refreshFontSettings();
            window.close();
        });
        buttonApply.setOnAction(event -> {
            Container.setFontSize(choiceBoxFontSize.getValue());
            Container.setSetWrapText(checkBoxWrapText.isSelected());
            Container.refreshFontSettings();
        });
        buttonCancel.setOnAction(event -> {
            window.close();
        });
    }

    public static void createStartLayout(){
        layoutStart = new GridPane();
        layoutStart.setAlignment(Pos.CENTER);
        Label labelHello = new Label();
        labelHello.setText("Hello");
        layoutStart.add(labelHello,0,0);
    }

//    public static boolean pressMainButton(String buttonName){
//        switch (buttonName){
//            case "Ok":
//                buttonOk.setOnAction(event -> tmp = true);
//                break;
//            case "Apply":
//                buttonApply.setOnAction(event -> tmp = true);
//                break;
//            case "Cancel":
//                buttonCancel.setOnAction(event -> System.exit(0));
//                break;
//        }
//        return tmp;
//    }
}
