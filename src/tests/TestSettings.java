package tests;

import javafx.application.Application;
import javafx.stage.Stage;
import windows.Settings;

/**
 * Created by HP on 23.02.2016.
 */
public class TestSettings extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        Settings.setSettings();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
