package kernel;

import windows.Main;

/**
 * Created by HP on 15.03.2016.
 */
public class Container {
    private static final int FONT_SIZE = 16;
    private static final boolean IS_WRAP = true;

    private static int fontSize = FONT_SIZE;
    private static  boolean setWrapText = IS_WRAP;

    public Container() {
        System.out.println("3243243423");
    }

    public static void refreshFontSettings(){
        Main.getMainTextArea().setStyle("-fx-font-size: " + fontSize);
        Main.getMainTextArea().setWrapText(setWrapText);
    }

    public static int getFontSize() {
        return fontSize;
    }

    public static void setFontSize(int fontSize) {
        Container.fontSize = fontSize;
    }

    public static boolean isSetWrapText() {
        return setWrapText;
    }

    public static void setSetWrapText(boolean setWrapText) {
        Container.setWrapText = setWrapText;
    }
}

