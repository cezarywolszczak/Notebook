package kernel;

import java.io.*;
import java.util.Scanner;

/**
 * Created by HP on 22.12.2015.
 *
 */
public class ReadFile {
    public static String readBinary(String fileName) {
        DataInputStream dataInputStream;
        String text = "";
        try {
            dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(fileName)));
            text = dataInputStream.readUTF();
        }catch (FileNotFoundException fnfe){
            System.err.println("Nie znaleziono pliku, ReadFile->readBinary");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String read(String fileName){
        String textLine = "";
        try {
            File file = new File(fileName);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()){
                textLine += scanner.nextLine()+"\n";
            }
        } catch (FileNotFoundException e) {
            System.err.println("Nie znaleziono pliku, ReadFile->readBinary");
        }
        return textLine;
    }
}
