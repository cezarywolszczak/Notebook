package windows;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class FunctionElements {
    private static TextField textFieldFind;

    public static Pane layoutFind(Button buttonClose, Button buttonNext) {
        HBox hBox = new HBox();
        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(0, 20, 0, 0));
        hBox.setPadding(new Insets(5, 20, 5, 20));
        hBox.getStylesheets().add("css/SettingStyles.css");
        hBox.setSpacing(10);

        Label labelFind = new Label("Find:");
        Label labelEmpyt = new Label("\t\t\t");

        textFieldFind = new TextField();

        buttonClose = new Button("X");
        buttonClose.setOnAction(event -> {
            Main.getLayout().setBottom(new GridPane());
        });
        buttonClose.styleProperty().bind(
            Bindings
                .when(buttonClose.hoverProperty())
                .then(
                    new SimpleStringProperty("" +
                            "-fx-font-weight: 900;" +
                            "-fx-font-size: 12;" +
                            "-fx-background-color: #454545;" +
                            "-fx-text-fill: #ffffff;" +
                            "-fx-border-color: #ffffff;" +
                            "-fx-border-radius: 6;" +
                            "")
                ).otherwise(
                    new SimpleStringProperty("" +
                            "-fx-text-fill: #A7A7A7;" +
                            "-fx-font-weight: 900;" +
                            "-fx-font-size: 12;" +
                            "-fx-background-color: #454545;" +
                            "-fx-border-color: #A7A7A7;" +
                            "-fx-border-radius: 6;" +
                            "-fx-border-width: 1" +
                            "")
            )
        );

        buttonNext = new Button(">");
        buttonNext.styleProperty().bind(
            Bindings
                .when(buttonNext.hoverProperty())
                .then(
                    new SimpleStringProperty("" +
                            "-fx-font-weight: 900;" +
                            "-fx-font-size: 12;" +
                            "-fx-background-color: #454545;" +
                            "-fx-text-fill: #ffffff;" +
                            "-fx-border-color: #ffffff;" +
                            "-fx-border-radius: 6;" +
                            "")
                ).otherwise(
                    new SimpleStringProperty("" +
                            "-fx-text-fill: #A7A7A7;" +
                            "-fx-font-weight: 900;" +
                            "-fx-font-size: 12;" +
                            "-fx-background-color: #454545;" +
                            "-fx-border-color: #A7A7A7;" +
                            "-fx-border-radius: 6;" +
                            "-fx-border-width: 1" +
                            "")
            )
        );


        hBox.getChildren().addAll(labelFind);
        hBox.getChildren().add(textFieldFind);
        hBox.getChildren().add(buttonNext);
        hBox.getChildren().add(labelEmpyt);

        borderPane.setLeft(hBox);
        borderPane.setRight(buttonClose);
        BorderPane.setAlignment(buttonClose, Pos.CENTER_RIGHT);

//        //TODO
//        Main.getWindow().addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                System.out.println("Eloszka");
//                labelEmpyt.setMinWidth(Main.getLayout().getWidth()-320);
//            }
//        });

//        labelEmpyt.setMinWidth(Main.getLayout().getWidth()-320);
//        System.out.println(Main.getLayout().getWidth());

        return borderPane;
    }

    public static void find() {
        String text = Main.getMainTextArea().getText();
        String find = textFieldFind.getText();
        String count = "";
        int i = 0;
        int index;
        int startIndex = 0;
        while ((index = text.indexOf(find, startIndex)) != -1) {
            startIndex = index + find.length();
            i++;
            count = count + " " + index;
        }
    }


    public static TextField getTextFieldFind() {
        return textFieldFind;
    }

    public static void setTextFieldFind(TextField textFieldFind) {
        FunctionElements.textFieldFind = textFieldFind;
    }

}
