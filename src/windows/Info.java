package windows;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;

/**
 * Created by HP on 17.12.2015.
 */
public class Info {

    public static void displayAllert(String title, String message, int x, int y){
        Stage window = new Stage();
        window.getIcons().add(new Image("image/notebookIcon.png"));

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(x);
        window.setMinHeight(y);
        window.setMaxWidth(x);
        window.setMaxHeight(x);

        //info Label
        Label label = new Label();
        label.setText(message);
        label.setStyle("-fx-text-fill: #ffffff");

        //layout
        VBox layout = new VBox(20);
        layout.getChildren().add(label);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        scene.getStylesheets().add("css/Styles.css");
        window.setScene(scene);
        window.show();

    }
}
